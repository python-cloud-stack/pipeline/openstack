#!/bin/bash
echo $(hostname -I | cut -d\  -f1) $(hostname) | sudo -h 127.0.0.1 tee -a /etc/hosts
echo "nameserver 8.8.8.8" | sudo tee /etc/resolv.conf > /dev/null
echo "ubuntu ALL=(ALL:ALL) NOPASSWD: ALL" | sudo tee /etc/sudoers.d/ubuntu
sudo apt update && sudo apt upgrade -y
