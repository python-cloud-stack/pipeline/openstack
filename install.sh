#!/bin/bash

####################################################################################### ROOT
sudo apt update -y && sudo apt upgrade -y
sudo adduser --shell /bin/bash --home /opt/stack stack
echo "stack ALL=(ALL) NOPASSWD: ALL" | sudo tee /etc/sudoers.d/stack

####################################################################################### STACK
sudo apt install git -y
sudo git clone https://git.openstack.org/openstack-dev/devstack /opt/stack/devstack
sudo chown -R stack /opt/stack/devstack
cd /opt/stack/devstack
echo "[[local|localrc]]

# Password for KeyStone, Database, RabbitMQ and Service
ADMIN_PASSWORD=StrongAdminSecret
DATABASE_PASSWORD=\$ADMIN_PASSWORD
RABBIT_PASSWORD=\$ADMIN_PASSWORD
SERVICE_PASSWORD=\$ADMIN_PASSWORD

# Host IP - get your Server/VM IP address from ip addr command
HOST_IP=51.75.144.87" > local.conf
cd /opt/
chmod 755 stack
cd stack
chmod 755 devstack
cd devstack
./stack.sh
echo 0
